import { Component } from '@angular/core';
import { NavParams, NavController } from 'ionic-angular';
import { Carro } from '../../domain/carro/carro';
import { Acessorio } from '../../domain/carro/acessorio';
import { PrecoUtils } from '../../domain/preco/precoUtils';
import { CadastroPage } from '../cadastro/cadastro';

@Component({
  templateUrl: './escolha.html'
})
export class EscolhaPage {
  
  public carro: Carro;
  public acessorios: Acessorio[];
  private _precoTotal: number;

  constructor(public navParams: NavParams, public navCtrl: NavController) {

    let params = this.navParams.get('carroSelecionado');
    this.carro = new Carro(params.nome, params.preco);
    this._precoTotal = this.carro.preco;
    this.acessorios = [
      new Acessorio('Freio ABC', 1000),
      new Acessorio('Airbag', 2000),
      new Acessorio('Hill holder', 500),      
    ];
  }

  precoFormatado(valor) {
    return PrecoUtils.formataValorParaReal(valor);
  }

  get precoTotal() {
    return this._precoTotal;
  }

  atualizaAcessorio(acessorio, ligado) {
    this._precoTotal =
      (ligado)
      ? this._precoTotal + acessorio.preco 
      : this._precoTotal - acessorio.preco;
  }
  
  avancarAgendamento() {

    this.navCtrl.push(CadastroPage, {
      carro: this.carro,
      precoTotal: this.precoTotal
    });
  }
}
