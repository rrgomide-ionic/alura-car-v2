import { Component, OnInit } from "@angular/core";
import { Http } from "@angular/http";
import { NavController, LoadingController, AlertController } from "ionic-angular";
import { EscolhaPage } from '../escolha/escolha';
import { PrecoUtils } from '../../domain/preco/precoUtils';

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage implements OnInit {

  public carros;

  constructor(
    public navCtrl: NavController, 
    private _http: Http,
    private _loadingCtrl: LoadingController,
    private _alertCtrl: AlertController) {}
  
  ngOnInit() {
  
    let loader = this._loadingCtrl.create({
      content: 'Buscando novos carros. Por favor, aguarde...'
    });

    loader.present();

    this._http
      .get('https://aluracar.herokuapp.com/')
      .map(answer => answer.json())
      .toPromise()
      .then(carros => {
        this.carros = carros;
        loader.dismiss();
      })
      .catch(err => {
        console.log(err);
        loader.dismiss();
        this._alertCtrl
          .create({
            title: 'Falha na conexão',
            buttons: [{text: 'Entendi'}],
            subTitle: `Não foi possível obter a lista de carros. 
                       Por favor, tente novamente mais tarde.` 
          }).present();
      });
  }

  seleciona(carro) {

    //console.dir(carro);
    this.navCtrl.push(EscolhaPage, { carroSelecionado: carro});
  }

  precoFormatado(valor) {
    return PrecoUtils.formataValorParaReal(valor);
  }
}
