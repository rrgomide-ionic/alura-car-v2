import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Alert } from 'ionic-angular';
import { Carro } from '../../domain/carro/carro';
import { PrecoUtils } from '../../domain/preco/precoUtils';
import { Http } from '@angular/http';
import { HomePage } from '../home/home';
import { Agendamento } from '../../domain/agendamento/agendamento';
import { AgendamentoService } from '../../domain/agendamento/agendamento-service';

@Component({
  templateUrl: 'cadastro.html'
})
export class CadastroPage {

  private _alerta: Alert;

  public carro: Carro;
  public precoTotal: number;
  public agendamento: Agendamento;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private _http: Http,
    private _alertController: AlertController,
    private _agendamentoService: AgendamentoService
  ) {

    this.carro = this.navParams.get('carro');
    this.precoTotal = this.navParams.get('precoTotal');

    this.agendamento = new Agendamento(
      this.carro,
      this.precoTotal
    );

    this._alerta = this._alertController.create({
      title: 'Aviso',
      buttons: [{
        text: 'Ok',
        handler: () => this.navCtrl.setRoot(HomePage)
      }]
    });
  }

  precoFormatado(valor) {
    return PrecoUtils.formataValorParaReal(valor);
  }

  _getMelhorDataAgendamento() {

    let hoje = new Date();
    
    let amanha = 
      new Date(hoje.getFullYear(), hoje.getMonth(), hoje.getDate() + 1)

    return (hoje.getHours() >= 17)
            ? amanha.toISOString()
            : hoje.toISOString();
  }

  agendarTestDrive() {

    if(!this.agendamento.nome || this.agendamento.endereco || this.agendamento.email) {
      this._alertController.create({
        title: 'Atenção',
        subTitle: 'Os campos de nome, endereço e e-mail são obrigatórios!',
        buttons: [{text: 'OK'}]
      }).present();

      return;
    }

    this._agendamentoService.agendar(this.agendamento)
      .then(() => {
        this._alerta.setSubTitle('Agendamento realizado com sucesso!');
        this._alerta.present();
      })
      .catch((erro) => {
        this._alerta.setSubTitle('Erro no agendamento!');
        this._alerta.present();
      });
  }    
}
