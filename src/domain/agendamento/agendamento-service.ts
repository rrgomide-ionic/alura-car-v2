import { Http } from '@angular/http'; 
import { Agendamento } from './agendamento';
import { Injectable } from "@angular/core";

@Injectable()
export class AgendamentoService {

  constructor(
    private _http: Http
  ) {}

  agendar(agendamento: Agendamento) {

    let request = 
      `https://aluracar.herokuapp.com/salvarpedido?carro=${agendamento.carro.nome}&nome=${agendamento.nome}&preco=${agendamento.valor}&endereco=${agendamento.endereco}&email=${agendamento.email}&data=${agendamento.data}`;
  
    return this._http
      .get(request)
      .toPromise()
  }
}
