export class PrecoUtils {
  
  public static formataValorParaReal(valor): string {
    let real = 
      `${(valor.toLocaleString('pt-br', {
        style: 'currency',
        currency: 'BRL'
      }))}`;
    
    return real.substring(0, 2) + ' ' + real.substring(2);
  }
}
