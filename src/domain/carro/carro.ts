import { PrecoUtils } from '../preco/precoUtils';

export class Carro {

  private _nome: string;
  private _preco: number;

  constructor(_nome: string, _preco: number) {
    this._nome = _nome;
    this._preco = _preco;
  }

  get precoFormatado() {
    return PrecoUtils.formataValorParaReal(this._preco);
  }

  get nome() {
    return this._nome;
  }

  get preco() {
    return this._preco;
  }
}
